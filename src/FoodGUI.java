import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodGUI {
    private JPanel root;
    private JLabel Topllabel;
    private JLabel orderitem;
    private JButton sushiButton;
    private JButton omletButton;
    private JButton tonkatsuButton;
    private JButton pastaButton;
    private JButton friedRiceButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane info1;
    private JTextPane totalinfo;
    int Total = 0;

    void order(String food){

            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to order Tempura?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if(confirmation == 0){
                JOptionPane.showMessageDialog(null, "Order for "+ food +" received");
                Total += 500;
                info1.setText(info1.getText()+food+"\n");
                totalinfo.setText("Total:"+ Total + "yen");

        }
    }

    public FoodGUI() {
        sushiButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
            order("Sushi");

            }
        });
        tonkatsuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Tonkatsu");
            }
        });

        omletButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Omlet");
            }
        });

        pastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pasta");
            }
        });
        friedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("FriedRice");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int comfirmation2 = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(comfirmation2 == 0){
                    int comfirmation3 = JOptionPane.showConfirmDialog(null,
                            "Do you use PlasticBag?",
                            "TakeOutStyle",
                            JOptionPane.YES_NO_OPTION);
                    if(comfirmation3 == 0){
                        Total += 3;
                    }
                    JOptionPane.showMessageDialog(null,"Thank you. Total : " + Total + "yen");
                    JOptionPane.showMessageDialog(null,"Please put in Chopsticks & Forks yourself.");
                    Total = 0;
                    totalinfo.setText("Total " + Total + "yen");
                    info1.setText("");
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
